# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


@author: a-noda
"""
import numpy as np
def load(file,doNormalize=True):
    f=np.load(file)
    inputs=f['inputs']
    if doNormalize:
        m=np.mean(inputs,axis=0)
        inputs-=m    
        s=np.std(inputs,axis=0)
        s+=np.sort(s)[len(s)//10]
        #s+=np.mean(s)/10
        inputs/=s
    
    inputs=inputs.astype(np.float32)    
    tars=f['tars'].astype(np.int32) 
    tidx,=np.nonzero(np.arange(inputs.shape[0])%10 == 0)
    lidx,=np.nonzero(np.arange(inputs.shape[0])%10 != 0)
    
    
    Linputs=inputs[lidx,:]
    Tinputs=inputs[tidx,:]
    Ltars=tars[lidx]
    Ttars=tars[tidx]
    return Linputs,Ltars,Tinputs,Ttars




if __name__=='__main__':
    Linputs,Ltars,Tinputs,Ttars=load('data/data_e.npz')
    