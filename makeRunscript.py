#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


Created on Thu Dec 14 16:47:49 2017
@author: a-noda
"""

import itertools 
from arg import Args
args=Args()
nofGPU=4
nofProcess=20
#sufix=("1","2","3");
#sufix=("4","5","6");
sufix=("7","8","9");

Aw=("T","F");
#Drop=("0","0.2","0.5");
Drop=("0",);

#nMed=("10","100","300")
#nMed1=("10","100","1000")
#nMed=("100","300")
#nMed1=("300","1000")
nMed=("300","100")
nMed1=("2000","1000")


#nLayer=("4","6")
nLayer=("5","6")

#Lasso1=("0","1e-5","1e-4","1e-3")
#Lasso=("0","1e-6","1e-5","1e-4")
#Lasso1=("0","1e-4","1e-3")
#Lasso=("0","1e-5","1e-3")
Lasso1=("0","1e-3","1e-5",     "1e-4")
Lasso=("0","1e-5","1e-6")
Decay=("0",)
P=("0","3")
awLimit=("1e+10","0")
fn=["r"+str(i)+".sh" for i in range(nofProcess)]
f=[open(i,"w") for i in fn]
rf=open("run.sh","w")
rf.write("#!/bin/sh\n")
rf.write("\n".join(["./"+i+"&" for i in fn]))
rf.close()

count=0
for p in list(itertools.product(Aw,nMed,nMed1,nLayer,Lasso1,Lasso,Decay,P,awLimit,Drop,sufix)):
    args.args["Aw"]=p[0]
    args.args["nMed"]=p[1]
    args.args["nMed1"]=p[2]
    args.args["nLayer"]=p[3]
    args.args["Lasso1"]=p[4]
    args.args["Lasso"]=p[5]
    args.args["Decay"]=p[6]
    args.args["P"]=p[7]
    args.args["awLimit"]=p[8]
    if("D" in args.args): del args.args["D"]  #optional parameter 
    if(p[9] is not "0"):args.args["D"]=p[9] 
    s=p[10]
 
    if( (p[9] is not "0" ) and (args.args["Aw"] is "T")):continue
    if(int(args.args["nMed"]) > int(args.args["nMed1"]) ):continue
    if(args.args["Aw"]=="F" and args.args["P"]!="0"):continue
    if(args.args["Aw"]=="F" and args.args["awLimit"]!="0"):continue
    if(args.args["Aw"]=="T" and args.args["awLimit"]=="0"):continue



    dev= count%nofGPU
    cmd="./awLasso.py "+str(args)+" "+str(dev)+" "+s
    #print(cmd) 

    f[count%nofProcess].write(cmd+"\n")
    count+=1
print(count)

for fi in f:
    fi.close()
