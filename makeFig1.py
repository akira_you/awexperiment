#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


Created on Thu Dec  7 16:47:14 2017

@author: a-noda
"""
import indata
import scipy.stats as stats
import numpy as np

Linputs,Ltars,Tinputs,Ttars=indata.load("sim.npz",doNormalize=False)
 
ln=Linputs[Ltars==0,:]
lp=Linputs[Ltars==1,:]

d=stats.ttest_ind(ln,lp,axis=0)
p=d.statistic
p=d.pvalue

import matplotlib.pyplot as plt
d=-1*np.log(p)
plt.figure(0,figsize=(8, 4));
plt.clf();
plt.xlim(0,200)
plt.plot(d)
plt.ylabel("-log(p)")
plt.xlabel("Data index (Truncated 200 of 1000)")
plt.vlines([20,30,40,50,60,70,80,90,100,110,120,130], 0, np.max(d)*1.1, colors='gray', linestyles=':')

       
plt.pause(0.01)
plt.savefig("fig1.png")