#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


@author: a-noda
"""
class Args:        
    def fromStr(self,argStr):
        self.args={}
        try:
            for a in argStr.split("_"):
                k,v=a.split("=")
                self.args[k]=v
        except:
            pass

    def __init__(self,argStr=""):
        self.fromStr(argStr)
    
    def __str__(self):
        s=""
        for k in sorted(list(self.args.keys())):
            s+=k+"="+self.args[k]+"_"
        s=s[:-1]
        return s
    def __cast(self,v,retType):
        if(retType == "str"):return v;
        if(retType == "int"):return int(v)
        if(retType == "float"):return float(v)
        if(retType == "bool"):
            if(v[0]=="F" or v[0]=="f" or v[0]=="0"):return False
            return True
        
    def get(self,k,retType="str",default=None):
        if(k in self.args):
            return self.__cast(self.args[k],retType)
        self.args[k]=str(default)
        return self.__cast(default,retType)
    def constGet(self,k,retType="str",default=None):
        if(k in self.args):
            return self.__cast(self.args[k],retType)
        return self.__cast(default,retType)

if __name__ == '__main__':
    args=Args()
    argStr="devNo=1_useAw=1"
    
    args.fromStr(argStr)
    print(args)
    print(args.get("devNo","2"))
    print(args.get("devNoo","1"))
