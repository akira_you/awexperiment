# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


Created on Sun Dec 31 12:55:11 2017

@author: akira
"""

from arg import Args
import glob
import numpy as np
args=Args()
import matplotlib.pyplot as plt
plt.close('all')



def getModel(argStr,baseDir):
    inDir=baseDir+"/"+argStr
    model=np.load(inDir+"/model.npz")
    args.fromStr(argStr)

    if(args.args["Aw"]!="F"):
        model=model["aw"]
    else:            
        model=model["fc1_n2"]
    return model
    
def getData(baseDir):
    argStrList=[a.split("/")[2] for a in glob.glob(baseDir+"/*")]
    nofArg=len(argStrList)
    
    #Lean ac,loss ,test acc,loss
    score=np.empty((nofArg,4),dtype=np.float32)
    
    argStrs=[]
    aws=[]
    pscore=[]
    for i,argStr in enumerate(argStrList):
        args.fromStr(argStr)
        inDir=baseDir+"/"+argStr
        score[i,:]=np.load(inDir+"/score.npz")["score"][-1]
        argStrs.append(argStr)
        
        mode="F"
        #if("D" in args.args):mode="FD"
        if(args.args["Aw"]=="T"):
            mode="T"+args.args["P"]
            
        aws.append(mode)
    
        model=np.load(inDir+"/model.npz")
        if(args.args["Aw"]=="T"):
            model=model["aw"]
        else:            
            model=model["fc1_n2"]
        mm=model**0.5
        pscore.append(np.sum(mm[np.arange(20,131,10)]) /np.sum(mm) )
        
    argStrs=np.array(argStrs)
    aws=np.array(aws)

    ss=score[:,2]
    #ss=np.array(pscore)
    
    
    idx,=np.nonzero(aws=='T0')
    bestT0=idx[np.argmax(ss[idx])]    
    idx,=np.nonzero(aws=='T3')
    bestT3=idx[np.argmax(ss[idx])]    
    idx,=np.nonzero(aws=='F')
    bestF=idx[np.argmax(ss[idx])]


    aa=[argStrs[i] for i in [bestF,bestT0,bestT3]]
    print(aa)
    models=[getModel(a,baseDir) for a in aa]
    
    evalScores=[np.load(baseDir+"/"+a+"/score.npz")["score"][:,2] for a in aa ]

    return (models,evalScores)

datas=[getData(d) for d in ("ANA/BATCH7","ANA/BATCH8","ANA/BATCH9")]
scores=[d[1] for d in datas]
models=[d[0] for d in datas]


def showModel(model,isAw,title):
    plt.xlim(0,200)
    #plt.title(title)
    if(isAw):
        plt.ylabel("Aw coeff")
    else:            
        plt.ylabel("L2 norm of 1st layer weiht")
    
    for i in range(3):
        l=None
        if(i==0): l = title
        plt.vlines([20,30,40,50,60,70,80,90,100,110,120,130], 0, np.max(model[i])*1.1, colors='gray', linestyles=':')
        plt.plot(model[i],label= l,color="blue")

    plt.legend()    

plt.figure("models",figsize=(8, 8))
plt.clf()

plt.subplot(3,1,1)
plt.title("Feature extract result")
showModel([m[0] for m in models],False,"No AW" )
plt.subplot(3,1,2)
showModel([m[1] for m in models],True,"AW" )
plt.subplot(3,1,3)
showModel([m[2] for m in models],True,"AW+SI")
plt.xlabel("Data order index (Truncated 200 of 1000)")

plt.savefig("fig2_models.png")
plt.pause(0.001)


plt.figure("Eval-acc");
plt.clf()
for i in range(3):
    plt.plot(scores[i][0],label="No AW",color="green")
    plt.plot(scores[i][1],label="AW",color="red")
    plt.plot(scores[i][2],label="AW +PI",color="blue")
    if(i==0):plt.legend()


plt.title("Accuracy (Test)")
plt.xlabel("epoch")
plt.ylabel("Accuracy")
plt.savefig("Eval-acc.png")
plt.pause(0.01)