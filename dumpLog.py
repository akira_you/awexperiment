#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


Created on Mon Dec 25 11:28:46 2017

@author: a-noda
"""

from arg import Args
import glob
import json
import numpy as np
import os
args=Args()

for baseDir in ("BATCH7","BATCH8","BATCH9"):
    fc1Key="updater/model:main/predictor/fc1/W"
    awKey="updater/model:main/predictor/aw/aw"
    argStrList=[a.split("/")[1] for a in glob.glob(baseDir+"/*/END")]
    
    for argStr in argStrList:
        outDir="ANA"+"/"+baseDir+"/"+argStr
        inDir=baseDir+"/"+argStr
        
        os.makedirs(outDir,exist_ok=True)
        log=json.load( open(inDir+"/log"))
        lastRes=log[-1]
        
        score=np.empty( (len(log),4),dtype=np.float32)
        for i in range(len(log)):
            score[i,0]=log[i]["main/accuracy"]
            score[i,1]=log[i]["main/loss"]
            score[i,2]=log[i]["validation/main/accuracy"]
            score[i,3]=log[i]["validation/main/loss"]
        snapShots=glob.glob(inDir+"/snapshot_iter_*")
        modelFile=snapShots[np.argmax([ int(s.split('_')[-1]) for s in snapShots])]
        
        model=np.load(modelFile)
        if(awKey in model.files):
            aw=model[awKey]
        else:
            aw=None
        fc1=model[fc1Key]
        fc1_n2=np.linalg.norm(fc1,ord=2,axis=0)
        fc1_n1=np.linalg.norm(fc1,ord=1,axis=0)
        
        np.savez_compressed(outDir+"/model",fc1_n1=fc1_n1,fc1_n2=fc1_n2,aw=aw)
        np.savez_compressed(outDir+"/score",score=score)



print("END")
