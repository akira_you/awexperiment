#!/home/a-noda/.pyenv/shims/python
# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  

Created on Mon May 30 20:24:39 2016
Last modified 2017/12
@author: a-noda
"""

import sys
from arg import Args
import os


batchMode=False
argStr=""
if(len(sys.argv) >1):
    argStr=sys.argv[1]
    batchMode=True

    
devNo=0
if(len(sys.argv) >2):
    devNo=int(sys.argv[2])
    
sufix=""
if(len(sys.argv) >3):
    sufix=sys.argv[3]
    
    
print("Run on ",devNo)
print("ARG:",argStr)
args=Args(argStr)
useAw=args.get("Aw","bool","T")
n_med=args.get("nMed","int",100)
n_med1st=args.get("nMed1","int",2000)
n_medlayer=args.get("nLayer","int",6)
lassoRate1st=args.get("Lasso1","float","1e-5" if useAw else "1e-5" )
lassoRate=args.get("Lasso","float","1e-5" if useAw else "1e-6")
decayRate=args.get("Decay","float",0)
pthresh=args.get("P","float",3)
awLimit=args.get("awLimit","float","1e+10")
dropOut=args.constGet("D","float","-1")
argStr=str(args)
resultDir="BATCH"+sufix+"/"+argStr
if(not batchMode):
    resultDir="MANUAL/"+argStr


print(resultDir)
if(batchMode and os.path.exists(resultDir+"/END")):
    print("==============Already have ",resultDir+"/END")
    sys.exit(1)




import numpy as np
import chainer
import chainer.functions as F
import chainer.links as L
import matplotlib.pyplot as plt
from chainer import cuda, optimizers,Chain,training
from awLayer import Aw,Aw_hook,zeroOneGv
#import gpu
#devNo=gpu.getGPU()






cuda.get_device(devNo).use()

##
#SETTINGS 
##        


#n_epoch = 2000
#n_epoch = 5000
n_epoch = 7000
awStartIteration=0
awCount=30 #very important factor trade off of speed and search space 
batchsize=800
       
class MyChain(Chain): 
    def __init__(self,n,useAw):
        self.n_input=n
        self.n_med=n_med
        self.n_medlayer=n_medlayer
        self.useAw=useAw
        super().__init__(
            fc1 = L.Linear(self.n_input,n_med1st),
            fc2 = chainer.ChainList(),
            fc3 = L.Linear(n_med,2),
        )
        for i in range(self.n_medlayer):
            self.fc2.add_link(L.Linear(None,self.n_med))
        if(self.useAw):
            self.add_link("aw",Aw(self.n_input))
            self.aw.updateRate=0.1#05#0.01 #very important factor  tradeoff  of speed and search space
            self.aw.L1decay=lassoRate1st
            self.aw.awStartIteration=awStartIteration
            self.aw.pthresh=pthresh
            self.aw.pthresh_base=-0.5
            self.aw.skipBackward=False
            self.aw.limit=awLimit
      
    def __call__(self, h):
        if(dropOut>0):
            h=F.dropout(h,ratio=dropOut)
        if(self.useAw):h=self.aw(h)
        h=self.fc1(h)
        h=F.elu(h)
    
        h=F.dropout(h)
        for f in self.fc2:
            h=f(h)
            h=F.elu(h)
        h=self.fc3(h)
        h=F.elu(h)
        return h
import cupy
from chainer.training import make_extension
@make_extension((1, 'epoch'))
def reportFC1(trainer):
    fc1=trainer.updater._optimizers['main'].target.predictor.fc1
    plt.figure("fc11")
    plt.clf()
    plt.xlim(0,200)
    plt.plot(cuda.to_cpu(cupy.linalg.norm(fc1.W.data,axis=0)).transpose())
    plt.pause(0.1)




if __name__=='__main__':

    import indata
    Linputs,Ltars,Tinputs,Ttars=indata.load("sim.npz",doNormalize=False)
   
    trainData=chainer.datasets.TupleDataset(Linputs,Ltars)
    testData =chainer.datasets.TupleDataset(Tinputs,Ttars)

    chain=MyChain(Linputs.shape[1],useAw)
    model=L.Classifier(chain)
    model.to_gpu()
    optimizer = optimizers.Adam()
    optimizer.use_cleargrads()
    optimizer.setup(model)
    #model.predictor.fc1.W.update_rule.add_hook(chainer.optimizer.GradientNoise(1.0))
    if(not useAw):
        model.predictor.fc1.W.update_rule.add_hook(chainer.optimizer.Lasso(lassoRate1st))

    optimizer.add_hook(chainer.optimizer.WeightDecay(decayRate))
    optimizer.add_hook(chainer.optimizer.Lasso(lassoRate))
    train_iter = chainer.iterators.SerialIterator(trainData, batchsize) 
    acc_iter=chainer.iterators.SerialIterator(Linputs,batchsize)
    test_iter = chainer.iterators.SerialIterator(testData, batchsize,repeat=False) 
    updater = training.StandardUpdater(train_iter,optimizer,device=devNo)
    trainer = training.Trainer(updater,stop_trigger= (n_epoch, 'epoch'),out=resultDir)
 

    if(useAw):
        #In case of Binary discrimination gv can be constant value
        gv=zeroOneGv(batchsize)
        class TrainToProp:
            def __init__(self,trainer):
                #fc1=trainer.updater._optimizers['main'].target.predictor.fc1.W
                #fc1.data+100e-4*cupy.random.randn(fc1.data.shape[0],fc1.data.shape[1],dtype=fc1.data.dtype)

                self.aw =    trainer.updater._optimizers['main'].target.predictor.aw 
                self.model = trainer.updater._optimizers['main'].target.predictor
                self.y =     trainer.updater._optimizers['main'].target.y
                #if you want to make gv from data(i.e. 3or more class )
                #use targetToGv(trainer.updater._optimizers['main'].target.loss.creator.inputs[1].data, nofdata)
                self.gv=  gv
                self.nextW=None
                #self.nextW= trainer.updater._optimizers['main'].target.predictor.fc1.W
                if('main/accuracy' in trainer.observation):
                    newGamma=0.1+trainer.observation['main/accuracy'].data                
                    newGamma**=0.5
                    #newGamma=newGamma+0.1
                    if(newGamma>0.9999):newGamma=0.9999
                    if(newGamma<0.5):newGamma=0.5
                    self.aw.gamma=newGamma *0.3+0.7*self.aw.gamma
                else:
                    self.aw.gamma=0.5
            
                # self.gv=targetToGv(trainer.updater._optimizers['main'].target.loss.creator.inputs[1].data,2)
        aw_hook=Aw_hook(TrainToProp,startIte=awStartIteration,awIteSpan=awCount,withGraph=0 if batchMode else 20)
        #This extention must run before Evalutor. Becaouse aw_hoook reuse the y(forward result)            
        trainer.extend(make_extension((1, 'iteration'))(aw_hook) ,priority=chainer.training.PRIORITY_WRITER+99)        
    
    import chainer.training.extensions as extensions
    trainer.extend(extensions.LogReport())
    trainer.extend(extensions.Evaluator(test_iter, model, device=devNo))
    trainer.extend(extensions.PrintReport(['epoch', 'main/accuracy', 'validation/main/accuracy' ,'main/loss' ,'validation/main/loss' ,'elapsed_time']))
    trainer.extend(extensions.snapshot(),trigger=(100, 'epoch'))
    if( (not useAw) and (not batchMode) ):trainer.extend(reportFC1)
    
    #To load some network do like here.
    #chainer.serializers.load_npz("BATCH/Aw_T_Decay_0_Lasso_1e-5_Lasso1_1e-5_P_3_awLimit_1e+10_nLayer_6_nMed_100_nMed1_1000/snapshot_iter_338",trainer)
    
    trainer.run()
    from pathlib import Path
    Path(trainer.out+'/END').touch()
    
    fc1=trainer.updater._optimizers['main'].target.predictor.fc1
    plt.figure("fc11")
    plt.clf()
    plt.plot(cuda.to_cpu(fc1.W.data).transpose())
    plt.savefig(trainer.out+"/fc1.png");
    if(useAw):
        aw=trainer.updater._optimizers['main'].target.predictor.aw
        plt.figure("fc11")
        plt.clf()
        plt.plot(cuda.to_cpu(aw.aw).transpose())
        plt.savefig(trainer.out+"/aw.png");
        


