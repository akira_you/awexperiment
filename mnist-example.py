#!/usr/bin/env python
#
#example implimentation for adding AW layer to mid-layer
#It's also example for multi-label classification
#

from __future__ import print_function

import argparse

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
from chainer.training import make_extension
useAw=True
awStartIteration=0
pthresh=3
awLimit=1e+10
awCount=100

from awLayer import Aw,Aw_hook,targetToGv

# Network definition
class MLP(chainer.Chain):
    def __init__(self, n_units, n_out):
        self.useAw=useAw
        self.n_units=n_units
        super(MLP, self).__init__()
        with self.init_scope():
            # the size of the inputs to each layer will be inferred
            self.l1 = L.Linear(None, n_units)  # n_in -> n_units
            self.l2 = L.Linear(None, n_units)  # n_units -> n_units
            self.l2_1 = L.Linear(None, n_units)  # n_units -> n_units
            self.l3 = L.Linear(None, n_out)  # n_units -> n_out

        if(self.useAw):
            self.add_link("aw",Aw(self.n_units))
            self.aw.updateRate=0.1#05#0.01 #very important factor  tradeoff  of speed and search space
            self.aw.L1decay=lassoRateAw
            self.aw.awStartIteration=awStartIteration
            self.aw.pthresh=pthresh
            self.aw.pthresh_base=-0.5
            self.aw.skipBackward=False
            self.aw.limit=awLimit
    def __call__(self, x):
        h = F.relu(self.l1(x))
        if(self.useAw):h=self.aw(h)
        h = F.relu(self.l2_1(h))
        h = F.relu(self.l2(h))
        return self.l3(h)

import matplotlib.pyplot as plt
import cupy
@make_extension((1, 'epoch'))
def reportFC1(trainer):
    fc1=trainer.updater._optimizers['main'].target.predictor.l1
    plt.figure("fc11")
    plt.clf()
    plt.plot(chainer.cuda.to_cpu(cupy.linalg.norm(fc1.W.data,axis=1)).transpose())
    plt.pause(0.1)
    

def main():
    global useAw,lassoRateAw
    parser = argparse.ArgumentParser(description='Chainer example: MNIST')
    parser.add_argument('--batchsize', '-b', type=int, default=1000,
                        help='Number of images in each mini-batch')
    parser.add_argument('--epoch', '-e', type=int, default=1000,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--frequency', '-f', type=int, default=-1,
                        help='Frequency of taking a snapshot')
    parser.add_argument('--gpu', '-g', type=int, default=0,
                        help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--out', '-o', default='result',
                        help='Directory to output the result')
    parser.add_argument('--resume', '-r', default='',
                        help='Resume the training from snapshot')
    parser.add_argument('--unit', '-u', type=int, default=200,
                        help='Number of units')
    parser.add_argument('--noplot', dest='plot', action='store_false',
                        help='Disable PlotReport extension')
    parser.add_argument('--aw', '-a', type=int, default=1,
                        help='use AW layer boolesan [0/1]')
    args = parser.parse_args()

    print('GPU: {}'.format(args.gpu))
    print('# unit: {}'.format(args.unit))
    print('# Minibatch-size: {}'.format(args.batchsize))
    print('# epoch: {}'.format(args.epoch))
    print('')
    useAw = args.aw!=0
    print("useAw",useAw)
    if(useAw):
        lassoRateAw=1e-5
        lassoRate1st=1e-4
        lassoRate=1e-6
        decayRate=0*1e-5#1e-4
    else:
        lassoRate1st=1e-4# no effect
        lassoRate=1e-5
        decayRate=1e-4


    if(useAw):
        #In case of Binary discrimination gv can be constant value
        class TrainToProp:
            def __init__(self,trainer):
                #fc1=trainer.updater._optimizers['main'].target.predictor.fc1.W
                #fc1.data+100e-4*cupy.random.randn(fc1.data.shape[0],fc1.data.shape[1],dtype=fc1.data.dtype)
    
                self.aw =    trainer.updater._optimizers['main'].target.predictor.aw 
                self.model = trainer.updater._optimizers['main'].target.predictor
                self.y =     trainer.updater._optimizers['main'].target.y
                #if you want to make gv from data(i.e. 3or more class )
                #use targetToGv(trainer.updater._optimizers['main'].target.loss.creator.inputs[1].data, nofdata)
                target=trainer.updater._optimizers['main'].target.loss.creator.inputs[1].data
                self.gv=  targetToGv(target, 10)
                self.nextW=None
                #self.nextW= trainer.updater._optimizers['main'].target.predictor.fc1.W
                if('main/accuracy' in trainer.observation):
                    newGamma=0.1+trainer.observation['main/accuracy'].data                
                    newGamma**=0.5
                    #newGamma=newGamma+0.1
                    if(newGamma>1):newGamma=1
                    if(newGamma<0.5):newGamma=0.5
                    self.aw.gamma=newGamma *0.3+0.7*self.aw.gamma
                else:
                    self.aw.gamma=0.5
                





    # Set up a neural network to train
    # Classifier reports softmax cross entropy loss and accuracy at every
    # iteration, which will be used by the PrintReport extension below.
    model = L.Classifier(MLP(args.unit, 10))
    if args.gpu >= 0:
        # Make a specified GPU current
        chainer.backends.cuda.get_device_from_id(args.gpu).use()
        model.to_gpu()  # Copy the model to the GPU

    # Setup an optimizer
    optimizer = chainer.optimizers.Adam()
    optimizer.setup(model)
    optimizer.add_hook(chainer.optimizer.Lasso(lassoRate))
    optimizer.add_hook(chainer.optimizer.WeightDecay(decayRate))

    # Load the MNIST dataset
    train, test = chainer.datasets.get_mnist()

    train_iter = chainer.iterators.SerialIterator(train, args.batchsize)
    test_iter = chainer.iterators.SerialIterator(test, args.batchsize,
                                                 repeat=False, shuffle=False)

    # Set up a trainer
    updater = training.StandardUpdater(
        train_iter, optimizer, device=args.gpu)
    trainer = training.Trainer(updater, (args.epoch, 'epoch'), out=args.out)

    # Evaluate the model with the test dataset for each epoch
    trainer.extend(extensions.Evaluator(test_iter, model, device=args.gpu))

    # Dump a computational graph from 'loss' variable at the first iteration
    # The "main" refers to the target link of the "main" optimizer.
    trainer.extend(extensions.dump_graph('main/loss'))

    # Take a snapshot for each specified epoch
    frequency = args.epoch if args.frequency == -1 else max(1, args.frequency)
    trainer.extend(extensions.snapshot(), trigger=(frequency, 'epoch'))

    # Write a log of evaluation statistics for each epoch
    trainer.extend(extensions.LogReport())

    # Save two plot images to the result dir
    if args.plot and extensions.PlotReport.available():
        trainer.extend(
            extensions.PlotReport(['main/loss', 'validation/main/loss'],
                                  'epoch', file_name='loss.png'))
        trainer.extend(
            extensions.PlotReport(
                ['main/accuracy', 'validation/main/accuracy'],
                'epoch', file_name='accuracy.png'))

    # Print selected entries of the log to stdout
    # Here "main" refers to the target link of the "main" optimizer again, and
    # "validation" refers to the default name of the Evaluator extension.
    # Entries other than 'epoch' are reported by the Classifier link, called by
    # either the updater or the evaluator.

    trainer.extend(extensions.PrintReport(
        ['epoch', 'main/loss', 'validation/main/loss',
         'main/accuracy', 'validation/main/accuracy', 'elapsed_time']))
    if(not useAw ):trainer.extend(reportFC1)
    model.predictor.l1.W.update_rule.add_hook(chainer.optimizer.Lasso(lassoRate1st))

    # Print a progress bar to stdout
    #trainer.extend(extensions.ProgressBar())
    if(useAw):
        aw_hook=Aw_hook(TrainToProp,startIte=awStartIteration,awIteSpan=awCount,withGraph=2)
        #This extention must run before Evalutor. Becaouse aw_hoook reuse the y(forward result)            
        trainer.extend(make_extension((1, 'iteration'))(aw_hook) ,priority=chainer.training.PRIORITY_WRITER+99)        

    if args.resume:
        # Resume from a snapshot
        chainer.serializers.load_npz(args.resume, trainer)
    
    # Run the training
    trainer.run()
    model.to_cpu() # CPUで計算できるようにしておく
    chainer.serializers.save_npz("mnist-model.npz" if useAw else "mnist-modelL.npz" , model) #

if __name__ == '__main__':
    main()
