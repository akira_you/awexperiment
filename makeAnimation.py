# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


Created on Sun Dec 31 12:55:11 2017

@author: akira
"""

from arg import Args
import glob
import numpy as np
args=Args()
import matplotlib.pyplot as plt
plt.close('all')



def getModel(argStr,baseDir):
    inDir="ANA/"+baseDir+"/"+argStr
    model=np.load(inDir+"/model.npz")
    args.fromStr(argStr)

    if(args.args["Aw"]!="F"):
        model=model["aw"]
    else:            
        model=model["fc1_n2"]
    return model
    
def getData(baseDir):
    argStrList=[a.split("/")[2] for a in glob.glob("ANA/"+baseDir+"/*")]
    nofArg=len(argStrList)
    
    #Lean ac,loss ,test acc,loss
    score=np.empty((nofArg,4),dtype=np.float32)
    
    argStrs=[]
    aws=[]
    pscore=[]
    for i,argStr in enumerate(argStrList):
        args.fromStr(argStr)
        inDir="ANA/"+baseDir+"/"+argStr
        score[i,:]=np.load(inDir+"/score.npz")["score"][-1]
        argStrs.append(argStr)
        
        mode="F"
        #if("D" in args.args):mode="FD"
        if(args.args["Aw"]=="T"):
            mode="T"+args.args["P"]
            
        aws.append(mode)
    
        model=np.load(inDir+"/model.npz")
        if(args.args["Aw"]=="T"):
            model=model["aw"]
        else:            
            model=model["fc1_n2"]
        mm=model**0.5
        pscore.append(np.sum(mm[np.arange(20,131,10)]) /np.sum(mm) )
        
    argStrs=np.array(argStrs)
    aws=np.array(aws)

    ss=score[:,2]
    #ss=np.array(pscore)
    
    
    idx,=np.nonzero(aws=='T0')
    bestT0=idx[np.argmax(ss[idx])]    
    idx,=np.nonzero(aws=='T3')
    bestT3=idx[np.argmax(ss[idx])]    
    idx,=np.nonzero(aws=='F')
    bestF=idx[np.argmax(ss[idx])]


    aa=[argStrs[i] for i in [bestF,bestT0,bestT3]]
    print(aa)
    models=[getModel(a,baseDir) for a in aa]
    
    evalScores=[np.load("ANA/"+baseDir+"/"+a+"/score.npz")["score"][:,2] for a in aa ]

    return (models,evalScores,[baseDir,aa])

datas=[getData(d) for d in ("BATCH7","BATCH8","BATCH9")]
scores=[d[1] for d in datas]
models=[d[0] for d in datas]
files=[d[2] for d in datas]
import json
import os
def fileToModelLog(file):
    fc1Key="updater/model:main/predictor/fc1/W"
    awKey="updater/model:main/predictor/aw/aw"
    ret=[]
    elogRet=[]
    for argStr in file[1]:
        inDir= file[0]+"/"+argStr
        mlog=[]
        elog=[]
        for j in  json.load( open(inDir+"/log")):
            i=j["iteration"]
            snapFile=inDir+"/snapshot_iter_"+str(i)
            if(not os.path.exists(snapFile)):continue
            elog.append(j["epoch"])
            model=np.load(snapFile)
            if(awKey in model.files):
                aw=model[awKey]
            else:
                aw=None
            fc1=model[fc1Key]
            fc1_n2=np.linalg.norm(fc1,ord=2,axis=0)
            m=fc1_n2 if(aw is None) else aw
            mlog.append(m)
        ret.append(mlog)
        elogRet.append(elog)
    return (np.array(ret),elogRet)
        
modelLogs=[ fileToModelLog(f) for f in files ]

def showModel(model,isAw,title,color):
    plt.xlim(0,200)
    #plt.title(title)
    if(isAw):
        plt.ylabel("Aw coeff")
    else:            
        plt.ylabel("L2 norm of 1st layer's weiht")
    
    for i in range(3):
        l=None
        if(i==0): l = title
        plt.vlines([20,30,40,50,60,70,80,90,100,110,120,130], 0, np.max(model[i])*1.1, colors='gray', linestyles=':')
        plt.plot(model[i],label= l,color=color)

    plt.legend()    


for i,epoch in enumerate(modelLogs[0][1][0]):
    print(epoch)
    mm=[m[0][:,i,:] for m in  modelLogs]
    
    plt.figure("models",figsize=(8, 10))
    plt.clf()
    plt.subplot(4,1,1)
    plt.title("Feature extract result")
    showModel([m[0,:] for m in mm],False,"No AW(Lasso)","green" )
    plt.xlabel("Data order index (Truncated 200 of 1000)")
    plt.subplot(4,1,2)
    showModel([m[1,:] for m in mm],True,"AW","red" )
    plt.xlabel("Data order index (Truncated 200 of 1000)")
    plt.subplot(4,1,3)
    showModel([m[2,:] for m in mm],True,"AW+SI","blue")
    plt.xlabel("Data order index (Truncated 200 of 1000)")

    plt.subplot(4,1,4)
    plt.subplots_adjust(hspace=0.5)
    
    for j in range(3):
        plt.plot(scores[j][0][:epoch],label="No AW",color="green")
        plt.plot(scores[j][1][:epoch],label="AW",color="red")
        plt.plot(scores[j][2][:epoch],label="AW +PI",color="blue")
        #if(i==0):plt.legend()
    
    plt.title("Accuracy (on Test Data)")
    plt.xlabel("epoch")
    plt.ylabel("Accuracy")
    plt.savefig("a"+("%04d"%i)+".png")
    plt.pause(0.001)
