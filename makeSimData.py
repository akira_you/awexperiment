# -*- coding: utf-8 -*-
"""
Copyrihgt Shimadzu Corp.
This demonstration code can only be used for research purposes 
(Includes academic education). Commercial use is prohibited. 
Distribution of derivative software  for your research is permitted 
if the distribution is limited to research use. 
Other redistribution is prohibited.
Contact: a-noda@shimadzu.co.jp  


@author: a-noda
"""

import numpy as np
data=np.random.randn(3*10000,1000).astype(np.float32)

y=0
y+=(data[:,20]+0.5)*(data[:,30]-0.4)* (data[:,40]+0.3) 
y+=(data[:,50]+0.25)*(data[:,60]-0.15)* (data[:,70]+0.05) 
y+=(data[:,80]+0.3)*(data[:,90]-0.2)* (data[:,100]+0.1) 
y+=(data[:,110]-0.2)*(data[:,120]+0.1)* (data[:,130]-0.0) 
def norm(input):
    input-=np.median(input)
    input/=np.std(input)

tar=np.zeros(data.shape[0],dtype=np.int32)
tar[np.nonzero(y< 0)[0]]=1

np.savez("sim.npz",inputs=data,tars=tar)








